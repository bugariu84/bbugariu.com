import React, {Component} from 'react';
import CssBaseline from '@material-ui/core/CssBaseline'
import Header from './components/Header/Header'
import PersonalInfo from './components/PersonalInfo/PersonalInfo'
import Grid from "@material-ui/core/Grid";
import {Paper} from "@material-ui/core";
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';


const styles = themes => ({
    main: {
        maxWidth: '975px',
        borderRadius: '0',
        height: '100%',
        margin: 'auto',
        border: 'none',
        overflow: 'hidden',
        padding: '60px 20px 60px 20px'
    }
});

class App extends Component {
    constructor(props) {
        super(props)
    }

    render() {
        const { classes } = this.props;
        return (
            <>
                <CssBaseline/>
                <Header/>
                <Paper className={classes.main}>
                    <Grid container spacing={16} alignItems="center">
                        <Grid item xs={12}>
                            <PersonalInfo />
                        </Grid>
                    </Grid>
                </Paper>
            </>
        );
    }
}


App.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(App);