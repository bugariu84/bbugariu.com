import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from "@material-ui/core/es/Toolbar/Toolbar";
import MenuIcon from "@material-ui/icons/Menu";
import {Typography, IconButton, Button} from "@material-ui/core";
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import styles from './styles/header'

class Header extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        const { classes } = this.props;
        console.log(classes);
        return (
            <header className={styles.header}>
                <AppBar position="static">
                    <Toolbar>
                        <IconButton>
                            <MenuIcon />
                        </IconButton>
                        <Typography variant="h6" color="inherit">
                            Title
                        </Typography>
                    </Toolbar>
                </AppBar>
            </header>
        )
    }
}

Header.propTypes = {
    classes: PropTypes.object.isRequired
};

export default withStyles(styles)(Header)