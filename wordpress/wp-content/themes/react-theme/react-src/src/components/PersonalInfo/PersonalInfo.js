import React from 'react';
import Grid from "@material-ui/core/Grid";
import Avatar from "@material-ui/core/Avatar";
import PropTypes from "prop-types";
import {withStyles} from "@material-ui/core";

const styles = theme => ({
    avatarHolder: {
        border: '1px solid red',
        textAlign: 'center'
    },
    avatar: {
        width: '152px',
        height: '152px',
        margin: '0 auto',
        [theme.breakpoints.down('sm')]: {
            width: '90px',
            height: '90px',
        },
        [theme.breakpoints.down('xs')]: {
            width: '45px',
            height: '45px',
        }
    }
})

class PersonalInfo extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        const {classes} = this.props;
        return (
            <Grid container>
                <Grid item xs={4}>
                    <Avatar className={classes.avatar} alt="Bogdan Bugariu 1 of the avatar image" src='http://dev-bbugariu.com/wp-content/uploads/2019/04/bbugariu.jpg'></Avatar>
                </Grid>
                <Grid items xs={6}>
                    <div>@NAME_AREA</div>
                    <div>@technologies info</div>
                    <div>@about_info</div>
                </Grid>
            </Grid>
        )
    }
}

PersonalInfo.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(PersonalInfo);