<?php
// This theme uses wp_nav_menu() in two locations.
register_nav_menus(
	array(
		'primary' => __( 'Primary Menu', 'react-theme' ),
		'social'  => __( 'Social Links Menu', 'rect-theme' ),
	)
);